# SPDX-FileCopyrightText: 2024 Blender Authors
#
# SPDX-License-Identifier: GPL-2.0-or-later

define HELP_TEXT
Convenience Targets
   Provided for building.

   * html:          Build HTML documentation (default).
   * clean:         Clean the build.

endef
# HELP_TEXT (end)

# Needed to ensure actions use this directory (especially removing files).
BASE_DIR := ${CURDIR}

.DEFAULT_GOAL := html

html: .FORCE
	@cd $(BASE_DIR) && mkdocs build

	@echo
	@echo "Docs built: ./site/index.html"

clean: .FORCE
	rm -rf $(BASE_DIR)/site

	@echo
	@echo "Docs cleaned from ./site/*"

export HELP_TEXT
help: .FORCE
	@echo "$$HELP_TEXT"

.FORCE:
