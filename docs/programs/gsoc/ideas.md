# Blender Summer of Code 2025 - Ideas

This page collects potential ideas and mentors for contributor projects
in the summer of 2025.

## Contacting us

You can contact us via our [developers
forum](https://devtalk.blender.org/c/contributing-to-blender/summer-of-code/15) or on
[chat.blender.org](https://chat.blender.org/#/room/#gsoc-2025:blender.org).
For matters you prefer to discuss in private, mail Thomas Dinges: thomas
at blender.org.

## General information for contributors and mentors

New to GSoC? Learn [how it works](https://summerofcode.withgoogle.com/how-it-works/) on the GSoC website.

The ideas list below provides ideas for projects that:

- Have an expected project size of ~90, ~175 or ~350 working
  hours.
- Implements a feature that developers have agreed would be a great
  improvement for Blender.

> INFO: **Submit your own proposal**
> Choosing an idea from this list is *not* mandatory. Contributors are
> encouraged to submit their own proposals or modified versions of the
> ideas from this page. To get an idea of what sort of project can be done for Blender and of
> the current development direction, see:
>
> - [Code Blog](https://code.blender.org/)
> - Design and todo tasks listed in the Workboards of [Blender modules](https://projects.blender.org/)
> - GSoC projects from previous years: [2024](2024.md) - [2023](2023.md) - [2022](2022.md) - [2021](2021.md) - [2020](2020.md) - [2019](2019.md)
> - [Release Notes](../../release_notes/index.md)

### Mentors

A mentor should be someone with Blender coding experience, who can guide
a contributor during all stages of the work, from design to
implementation.

### Contributors

Contributors who wish to apply should first carefully read the [Getting
Started page](getting_started.md) and check if all the
conditions to participate are met.

If by some reason you can not participate, your contribution outside of
the program is still welcome! Feel free to develop an idea from the page
as long as no contributor chose it.

We especially invite contributors to contribute based on their past
experience and competencies, or based on their current research.

Nearly all project ideas will require a strong working knowledge of C
and/or C++. In addition, we use Python for much of our interface code
(anyone skilled in C/C++ can trivially learn enough Python to make the
needed interface code changes). An exception to this are projects where
only Python is required.

> INFO: **Get Started**
>
> [:octicons-arrow-right-24: Getting Started Guide and Application Template](getting_started.md)


## Ideas

> INFO: **Work in progress**
>
> This list is work in progress, new ideas will be added

<!---
Template for new project ideas

### Category

#### Project Title

- *Description:* XXX
- *Expected outcomes:* XXX
- *Skills required:* XXX
- *Possible mentors:* XXX
- *Expected project size:* 90, 175 or 350 hours
- *Difficulty:* easy, medium or hard
-->


<!--- 2024 ideas for reference, remove later
### Flamenco

#### Improve Distributed Rendering & Task Execution

- *Description*: Flamenco is more popular than ever, but still missing certain
  key features. Some suggestions to work on are listed below. Note that these
  are of varying difficulty and size.
  - Convert the build toolchain to [Mage](https://magefile.org) to
    simplify development and packaging on Windows.
    ([#102633](https://projects.blender.org/studio/flamenco/issues/102633)
    and
    [#102671](https://projects.blender.org/studio/flamenco/issues/102671)).
  - Allow finishing setup assistant without Blender on Manager ([#100195](https://projects.blender.org/studio/flamenco/issues/100195)).
  - Introduce per-Worker logs on the Manager, for introspection and
    debugging.
  - Introduce RNA Overrides
    ([#101569](https://projects.blender.org/studio/flamenco/issues/101569)).
  - Create a web interface for Flamenco Manager configuration
    ([#99426](https://projects.blender.org/studio/flamenco/issues/99426)).
  - Create [job types](https://flamenco.blender.org/usage/job-types/) for
    various tasks, such as distributed rendering of single images, and more
    powerful control over parameters for studios.
  - Improve stability and predictability (for example [#99417](https://projects.blender.org/studio/flamenco/issues/99417)).
  - Use [Fyne](https://fyne.io/) to give Flamenco Manager and Worker an optional GUI for desktop use.
  - Add an icon & metadata to the Windows executables using [go-winres](https://github.com/tc-hib/go-winres).
  - There are [plenty of other things to work on](https://projects.blender.org/studio/flamenco/issues) if you want.
- *Expected outcomes*: Give users a simple way to distribute tasks, such as
  rendering, over multiple computers on their network.
- *Skills required*: Familiarity with Go. Depending on the exact tasks that
  will be performed, also knowledge of Blender/Python and web languages
  (HTML/CSS/JavaScript).
- *Possible mentors*: [Sybren Stüvel](https://projects.blender.org/dr.sybren/).
- *Expected project size*: 175 or 350 hours.
- *Difficulty*: medium to hard, depending on the chosen tasks.

### Geometry Nodes

#### Sample Sound Node

- *Description*: This node allows retrieving the volume of a sound in frequency range at a point in time. It allows e.g. animating the spectrum of a song but also more complex things when combined with e.g. a simulation zone. It's a relatively small node that opens up many new possibilities in geometry nodes.
- *Expected outcomes*: A new `Sample Sound` node that is highly flexible in its use and efficient (probably by doing a fair amount of caching).
- *Skills required*: Proficient in C/C++, good understanding of FFT
- *Possible mentors*: Jacques Lucke, Hans Goudey
- *Expected project size*: 175 hours
- *Difficulty*: medium

#### File Import Nodes

- *Description*: Dynamically loading file formats like STL, OBJ, Alembic and CSV with a geometry node would simplify common data visualization tasks. Also it would reduce memory consumption on disk because the data does not have to be stored in the .blend file.
- *Expected outcomes*: At least one new node that loads one of the file formats mentioned above. Depending on the complexity, more than one file format node should be added. The existing parsing code in Blender should be reused where appropriate. Some refactoring of existing parsing code may be required.
- *Skills required*: Proficient in C/C++
- *Possible mentors*: Jacques Lucke, Hans Goudey
- *Expected project size*: 175 hours
- *Difficulty*: medium

#### Spreadsheet Improvements

- *Description*: The current spreadsheet in Blender is very limited in functionality. It can be greatly improved by adding features like editing, improved filtering, sorting and exporting.
- *Expected outcomes*: New well designed functionality in the spreadsheet that makes it more useful for users.
- *Skills required*: Proficient in C/C++
- *Possible mentors*: Jacques Lucke, Hans Goudey
- *Expected project size*: 175 hours
- *Difficulty*: medium


### Modeling

#### UV Editor Improvements

- *Description:* A selection of some of the smaller user requests for UV
  editing tools would help users make better UV maps more easily. Some
  example ideas:
  - A tool to straighten selected edges into a line.
    [This request](https://blender.community/c/rightclickselect/mpfbbc/)
    shows some examples.
  - Adapting some more Mesh Edit functionality to the 2D UV editing case. For example,
    [Additional Snapping Targets](https://blender.community/c/rightclickselect/XQlg),
    [Mirror UV's](https://projects.blender.org/blender/blender/issues/78407).
- *Expected outcomes:* More tools to improve editing of UVs.
- *Skills required:* Proficient in C/C++, familiarity with vector math
- *Possible mentors:* Howard Trickey, Campbell Barton
- *Expected project size:* 350 hours
- *Difficulty:* medium

#### UV Stitch Tool Improvements

- *Description:* Support stitching between UV islands resulting in
  gap-less joins at boundaries. This would require fitting one UV island
  to another by deforming it, other tasks may include:
  - Parameters for fitting boundaries, to adjust the tolerance for the
    extents of the boundaries which are merged.
  - Parameters for UV island distortion to adjust how the surrounding
    UV's are adjusted for minimal distortion.
  - Potentially multiple methods to fit islands.
- *Expected outcomes:* Faster stitching of UV islands which are not
  exactly aligned, especially for organic models.
- *Skills required:* Proficient in C/C++, familiarity with vector math
- *Possible mentors:* Campbell Barton
- *Expected project size:* 350 hours
- *Difficulty:* medium

### VFX & Video

#### Sequencer improvements

- *Description:* A selection of various topics to improve the Video Sequencer:
  - Link strips: allow two strips to have strong bound between each other, like
    audio and video strips of the same input video file. Tools like transform,
    retime, cut, and others should handle linked strips together.
  - Better positioning of strip on adding and pasting. Currently the selection
    of a channel when adding new strip or pasting them from a clipboard feels
    random on user-level. The goal of this topic is to make this behavior clear
    and predictable.
  - Improved pitch management of audio strips: allow retiming with and without
    pitch correction.
  - Implement snapping support in the preview region of the sequencer.
- *Expected outcomes*: Give users a better experience when editing video and audio in Blenders Video Sequencer.
- *Skills required:* Proficient in C/C++, familiarity with vector math, DSP
- *Possible mentors:* Richard Antalik, Aras Pranckevicius
- *Expected project size:* 350 hours
- *Difficulty:* medium
-->
