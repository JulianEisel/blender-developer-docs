# A4 Design

Inspired by Toyota A3 process, the idea is to synthesize all the information relevant
to a design proposal in a piece of A4 (or 1920x1080).

* This prevents a more extensive presentation (e.g., slideshow)
  to get derailed because of small details.
* A good strategy is to organize the different sections in context,
  from the big picture to specific details.

## Examples

Those are a few examples of design documents that were presented to Ton Roosendaal
between 2021 and 2022.

### Spreadsheet Editor

Implemented in 2.93 LTS.

![Spreadsheet Editor](../../images/design_A4_spreadsheet_presentation.jpg){style="width:800px;"}

### Asset Bundle

Proposal, for Blender 3.0.

![Asset Bundle](../../images/design_A4_asset_bundle.jpg){style="width:800px;"}

### Hair

Incomplete A4 Design, in the end the information was better presented as a more complete
[blog post](https://code.blender.org/2022/07/the-future-of-hair-grooming/).

![Character Hair System](../../images/design_A4_hair.jpg){style="width:800px;"}
