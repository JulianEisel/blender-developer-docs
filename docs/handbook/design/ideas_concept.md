# Idea, Concept, Design

It is important to be able to tell an idea, a concept and a design apart.
They are to be treated differently and to be consulted on different stages of development.

## Ideas

A solution-driven suggestion, which can be used to find important problems to tackle.

Even when the specific solution may be up for debate, it can still help to show the feasibility of
having a solution to the problem, as well as its scope and cost.

The more the problem is framed, a big picture is presented and different scenarios are considered,
the more an idea gets closer to being a design.

See [rightclickselect.com](https://rightclickselect.com/) to see examples of Blender ideas.

## Concept

A visual mockup of how a design could look like.

To be referenced on the final stages of design, when there is a design in place and it is time to
see how things should look like.

When concepts are not following a proposed design, they are basically a visualization of an idea
and should be treated like any other ideas.

Concepts early in the design stage are useful to show that there are feasible solutions, and the
project won’t hit a dead-end down the line.

Fleshed out concepts are particularly important after a design is already in place. To explore UI
variations for prototypes and the final solution.

## Design

A comprehensive document highlighting the big picture, the problem it tries to solve, its target
audience, how it impacts all aspects of Blender, how it affects other user groups, and proposed
solutions.

Solutions should be framed from the outcome point of view.
And you should be able to connect the solution with its intended outcome
(e.g., “once Bone Collections are implemented, riggers will be able to organize
their rigs better by putting control bones and deform bone in separate
collections”).

While the discussion eventually is centered around the proposed solution, there needs to be first
agreement on all the other aspects. This is where a lot of times we jump the gun and discuss
different solutions to the problem, before even considering whether this is the correct problem we
should tackle.
