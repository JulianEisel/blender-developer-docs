# Communication and Presentation

## Communication

* Feedback thread
* Blog posts

## Presentation

* Diagramas
* Wireframes
* High-fidelity mockups
* [A4](a4_design.md)

To help with mockups Blender has a
[UI Toolkit](toolkit/index.md) built for [Penpot](https://www.penpot.app/).
