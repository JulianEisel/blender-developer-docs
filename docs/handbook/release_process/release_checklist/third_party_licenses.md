# Third Party Licenses

An important part of the [release process](index.md) is to
make sure [Blender is shipped with the complete license for all its
dependencies](https://code.blender.org/2021/05/blender-oss-and-licenses/).

Those dependencies come from a few places:

- `./extern/`
- `./intern/`
- `./lib/`: pre-compiled libraries from GIT-LFS.

It is not enough to list the licenses (MIT, GPL, ...). Often times the
license also require its copyright holders to me mentioned.

A [python script](https://projects.blender.org/blender/blender/src/branch/main/tools/utils_maintenance/make_license.py) is used to compile a
[license.md](https://projects.blender.org/blender/blender/src/branch/main/release/license/license.md) document
based on the dependency information available in the source code. This document is shipped together with Blender.
It lists all dependencies, sorted by their license and offers copyright and project information.

## Updating the license document

The `make_license.py` script can be run from the root source folder by running `make license`.
Warnings are shown if license or copyright information are missing.

>? INFO: **Verify the result**
>
> Even though the script is taking all license information from the source,
> always double check with the ground truth based on git history and the `Libraries Changes for Blender X.X` issue (linked from the milestones).

Commit the updated file to the repository.

## Script internals

The script takes its data mostly from
[versions.cmake](https://projects.blender.org/blender/blender/src/branch/main/build_files/build_environment/cmake/versions.cmake) for libraries in `./intern` and `./lib`
and from `README.blender` files in `./extern/`.

Each dependency should have the following variables:

| Value | cmake.versions  | REAMDE.blender  | Notes |
|---|---|---|---|
| Name | `[LIB]_NAME` | `Project:`|  A human readable name of the dependency |
| Link | `[LIB]_HOMEPAGE` | `URL:`|  Link to the project website or repository |
| License | `[LIB]_LICENSE` | `License:`|  A license identifier. Check [licenses.json](https://projects.blender.org/blender/blender/src/branch/main/release/license/licenses.json) for a list of available ones |
| Copyright | `[LIB]_COPYRIGHT` | `Copyright:`|  Copyright information |
