# Microsoft Store

Blender is available on [Microsoft Store](https://apps.microsoft.com/detail/9pp3c07gtvrh).

## Publishing Builds

Buildbot is creating an `.msix` package that can be uploaded to Microsoft Store.

- Run `vXXX-code-store-coordinator` to generate the `.msix` file
- Run `vXXX-code-artifacts-deploy-coordinator` to deploy files to download.blender.org.
- Login to the Microsoft Store and select the corresponding product you like to update.
  - `Blender` for the latest stable release
  - `Blender [X.X] LTS` for LTS releases
- Click on `Start update` and update the package and store listing.
- Finalize the submission by clicking on `Submit for certification`.

Once the new package has been submitted it takes anywhere between
several hours to several days for the submission to complete, after
which it can take up to 24 hours for the update to be available on all
Microsoft Store markets.

# LTS Releases

The first version of an LTS (e.g. `4.2.0`) is released onto the `Blender` product,
replacing the last stable release.
Only after the next non LTS version is out (e.g. `4.3.0`) and released on Microsoft store,
the next LTS update (e.g. `4.2.X`) gets released onto a new LTS product.

-  Log in to the Microsoft Store dashboard (access to shared
    credentials needed)
-  Create a new application
-  Name it along the form of `Blender X.YZ LTS`, i.e. `Blender 4.2 LTS`
-  When the first submission is created fill out all necessary
    information
  -  You can copy&paste most from the `Blender` application.
  -  Ensure the correct age badges are set, unfortunately one can't
      copy the ID from other applications.
  -  For the listing, export the one from the previous `Blender [X.X] LTS`,
      update it for the new one, and import it (en-US).
      Don't forget to upload the imagery for the listing.
  -  Ensure that the version mentioned throughout this new application is
      correct.

