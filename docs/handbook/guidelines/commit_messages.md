# Commit Message Guidelines

Please observe the following guidelines and provided templates for your
commits:

- The first line represents the subject of the commit.
- The body of the commit message is separated from the subject by a
  blank line.
- Lines should not exceed 72 characters.
- Do not embed images in the commit description.
- ASCII for diagrams and arrows can be used where appropriate.

Many git tools assume these metrics for proper rendering, such as
`git log --pretty=oneline` or `gitweb`. Automated reports or just plain
future development depend on good logs.

## Language

- Commit messages should use American English suitable for technical
  documentation.
- Abbreviations should only be used when they are well understood in the
  field.
- When naming other people in a patch, use their full name instead of
  nickname or username.

## Commit Types

Nearly all commits are one of the following 3 types, and must follow the
associated conventions.

### Bug Fixes

- Don't copy the bug report name verbatim if it's not descriptive,
  explain exactly what the problem was.
- Explain what you fixed on a user level, do not focus only on what was
  wrong in the code.
- Start the commit log with "Fix \#12345: " or "Fix:" (for unreported
  bugs), so it's immediately clear that it's a bug-fix.
- **Bonus Feature**: mentioning "Fix \#12345" will auto-close the bug
  \#12345 on projects.blender.org!

***Template:***

    Fix #12345: Single short line to explain on a user level what the bug was

    Optionally, more user level information about which scenarios the bug
    happened in, why it was fixed in this particular way, etc.

    If non-obvious, some technical note about what the cause of the bug was and
    how it was solved.

### New Features and Improvements

- Explain what the feature does on a user level, not just the code
  changes.
- Keep user level explanation and code changes explanation separate.
- Explain features in terms of their names in the user interface, not
  internal code terminology.
- If it's not obvious, explain what the feature is useful for or when it
  should be used.
- Start with a category like "Cycles:", "Sculpt:" or "UI:".
- For performance improvements, give specific numbers from tests rather
  than vague terms like "slightly faster".

***Template:***

    Category: Single short line saying what the feature you have implemented is

    More user level information about how this feature works, explanation about why
    it's good to have, link to docs or release notes, etc.
     
    Optionally short technical notes about how the feature was implemented.

Relevant bugs, design or to do issues can be auto closed or referenced
with respectively `Fix \#123` or `Ref \#123` at the end of the
commit message.

### Code Cleanups and Refactoring

- Use only when there are no functional changes expected.
- Separate cleanup commits from functional changes. First clean up the
  code, commit that as a cleanup commit, then commit the functional
  changes.
- Use the `Cleanup:` prefix when you remove unused code, fix compiler
  warnings, fix typos, etc.
- Use `Refactor:` when you restructure/move code (for better
  maintainability).

***Template:***

    Cleanup: Single short line describing what you cleaned up

    Optionally more information about the cleanup.

## Code Review

For code review, the recommended workflow is to use [pull
requests](../contributing/pull_requests.md).

Pushing directly to the repository is possible, and the pull request
link must be manually added in the commit message then. The pull request
will not automatically close.

## Author(s) / Committer

- If you commit a patch of your own, you are author and committer of the
  commit (the default).
- If you commit a patch on behalf of someone else, there are several
  cases:
  - If the full name of the single original author is known, you are
    committer, but the original author becomes author in git. Commit as
    `git commit --author "Full Name <Nick>"`.
  - Otherwise, you remain author and committer in git. However, you have
    to add a `Co-authored-by:` line to the commit message.
  - In case there are several co-authors to a commit, add one line
    starting with `Co-authored-by:` for each of them.

Using the [`Co-authored-by:`
convention](https://docs.github.com/en/pull-requests/committing-changes-to-your-project/creating-and-editing-commits/creating-a-commit-with-multiple-authors)
(one line per co-author) allows the tool generating credits (list of
contributors) for a release to automatically include all the co-authors
of a commit. Gitea will also automatically generate these lines from all
authors of the commits in a PR in case of a squashed commit. Note that
several variants of this formatting are supported:

- `Co-authored-by: Real Name`
- `Co-authored-by: Real Name <emailname@email.com>`
- `Co-authored-by: Real Name <NickName>`

In the rare case where there are some errors or missing author
information in a commit, it is possible to *fully overwrite* the
authors' information for that specific commit, for the generated credits
only. This requires editing the [credits tool
code](https://projects.blender.org/blender/blender/src/branch/main/tools/utils/credits_git_gen.py)
to add an entry in the `commit_authors_overwrite` dict.
