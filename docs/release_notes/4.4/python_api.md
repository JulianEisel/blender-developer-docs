# Blender 4.4: Python API & Text Editor

## Breaking Changes

### Blender-based Python Classes Construction

Python-defined classes based on Blender types (like `Operator`, `PropertyGroup`, etc.)
that define their own `__new__`/`__init__` constructors [must now call the parent's matching
function](https://docs.blender.org/api/4.4/info_overview.html#construction-destruction),
and pass on generic positional and keyword arguments:

```python
import bpy
class AwesomeRaytracer(bpy.types.RenderEngine):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        ...
```

### Paint

- `paint.brush` and `paint.eraser_brush` are now read-only properties. (blender/blender@9e8c037375b42f7e2376549e55958cb98c4eea4e)

### GPU

- When drawing with `POLYLINE_FLAT_COLOR`, `POLYLINE_SMOOTH_COLOR` or `POLYLINE_UNIFORM_COLOR` or
  when drawing wide lines (line width greater than 1) using `FLAT_COLOR`, `SMOOTH_COLOR` or
  `UNIFORM_COLOR`, the following rules now apply:

  - The `pos` attribute must use `F32` format `FLOAT` fetch type.
  - The `color` attribute must use `F32` format with `FLOAT` fetch type **or** use `U8` with
    **4 components** and `INT_TO_FLOAT_UNIT` for fetch mode.
  - Each attribute needs to be 4 byte aligned.
  - Primitive type needs to be `GPU_PRIM_LINES`, `GPU_PRIM_LINE_STRIP` or `GPU_PRIM_LINE_LOOP`.
  - If drawing using an index buffer, it must contain no primitive restart index.
  
### Grease Pencil

- The influence vertex group was removed from the Texture Mapping modifier (blender/blender@c452d5d9e807243ade36809b61673f590bdf67f7)

### Sequencer

- The `bpy.types.Sequence` and all related types got renamed to `bpy.types.Strip`. See the full list [here](sequencer.md#breaking-changes). 
- Text strip `align_x` and `align_y` were renamed to `anchor_x` and `anchor_y`. Property `alignment_x` does proper alignment now. (blender/blender@77a5478c0f5460b3284826c5f11f818c26f9823b)

## Additions

### Grease Pencil

The Grease Pencil Python API got some new functionality.
See the changes [here](grease_pencil.md#python-api).

### Curves

- New `Curves.reorder_curves(new_indices=[...])` to reorder curves.
  (blender/blender@a265b591bede2a6cf9cda189bee641cbbe94a5a9)
- New `Curves.set_types()` to change curve types
  (blender/blender@5db88ff2e3457763ceb6090378a5ccc8bb45ab48).

  
### Nodes
- New `Node.color_tag` property, which returns an enum item corresponding
  to the node color tag (Texture, Vector, Output, etc...)
  (blender/blender@6cd33510c33fe17e999f4cadf869fc19a637eecb)
- New `bl_use_group_interface` property on custom node trees that allows disabling some built-in UI for node groups (blender/blender@ebfbc7757b16e7bdfc87272e89584afb8f75a2a4).


## Blender as a Python Module

The bpy package on pypi now optionally provides access to VFX libraries used by Blender. (blender/blender!133082)

While most are available as separate packages on pypi, they may not have the same version or build options as Blender. This can lead to errors and crashes when interchanging data with Blender.

```python
import bpy

# Add Blender bundled VFX libraries to sys.path.
bpy.utils.expose_bundled_modules()

# Import the library you need.
import pxr
import MaterialX
import OpenImageIO
import PyOpenColorIO
import pyopenvdb 
```