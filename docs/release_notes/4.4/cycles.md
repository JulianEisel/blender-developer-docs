# Blender 4.4: Cycles

## Denoising
* Update the NVIDIA OptiX denoiser type, bringing improved denoising quality.
(blender/blender@793918f2b1ea0f46a8b0977fda46d508c87cfcf1)
  * Improvements in denoising quality can manifest as more consistent denoising,
  more accurate colours, better retention of detail, and less denoising blotching.
  In some situations denoising quality can decrease.
  * This change only impacts users with older GPU drivers as newer drivers have
  already switched to this denoiser type internally.

|Old Denoiser|New Denoiser (Scene by Metin Seven)|
|-|-|
|![](images/cycles_optix_denoiser_old.webp)|![](images/cycles_optix_denoiser_new.webp)|

## Sample Subset
To correctly distribute the blue noise sampling pattern across multiple computers,
the sample offset feature has been modified. There is now a Sample Subset toggle
with both an Offset and Sampling length. (blender/blender@f09fd9bdef3242a726667eba12f96514d33aba0a)

Scripts for distributed rendering using this feature need to be updated.

Please refer to the [Blender manual](https://docs.blender.org/manual/en/4.4/render/cycles/render_settings/sampling.html#advanced)
for details.

## Other
* On Linux, the minimum ROCm version required to use HIP on AMD GPUs has increased to 6.0.
(blender/blender@4e5a9c5dfb582b03d6e11d751e5a5c7e1bcdda9d)
