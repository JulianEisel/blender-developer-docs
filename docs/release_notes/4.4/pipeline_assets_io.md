# Blender 4.4: Pipeline, Assets & I/O

## USD
- Animated Volumes created procedurally in Geometry Nodes,
  or through the usage of other volume modifiers, are now supported during export
  (blender/blender@391612c7259f496d5176f1ce545d26f491490e7f)
- Material displacement support has been added for the UsdPreviewSurface
  during both import and export
  (blender/blender@b4c2feea38cb4a1a75d685d6b05c270af288c4db)
- Point instancers with time-varying (animated) attributes are now supported during import
  (blender/blender@2523958e0e3ec903c8e555baa01d985ea19384b1)
- Python hooks
  - Import: Added new `get_prim_map` API callable during `on_import`. Provides a mapping between USD prims and the Blender objects created for said prim.
  (blender/blender@0df5d8220b407fc6929302f1ca7040dd66476be7)
  - Import: Added new `on_material_import` API. Provides an opportunity to fully override material loading for certain material types; typically used for cases where the native material import either doesn't support the format or in other highly-specialized scenarios. (blender/blender@74512cc5cb93e63d5caaf400d0c5d2ccd1753e63)
  - General: Added support for `import_texture` and `export_texture` functions callable from `on_material_import` and `on_material_export` respectively (blender/blender@74512cc5cb93e63d5caaf400d0c5d2ccd1753e63)
- New Import options
  - Added a `Merge parent Xform` option: Control if USD prims merge with their Xform parent during
    Import in order to better preserve scenario-specific hierarchy requirements
    (blender/blender@000416c933de383d2a7dc8f5b56f76e2e4a33e41)
  - Added a `Apply Unit Conversion Scale` option: Scale the scene objects by the USD stage's meters per unit value. (blender/blender@0c544974d1690d8476031b9c6965f1920fb90494)
- New Export options
  - Added a `Merge parent Xform` option: Control if Blender object transforms are directly written
    to their data prim or kept separate on Export. This can reduce the number of USD prims in the
    Stage and better preserves scenario-specific hierarchy requirements
    (blender/blender@428ab699dcf58c607a8995b9d5e77b8ddb7e3679)
  - Added `Units` and `Meters Per Unit` options: Set the USD Stage meters per unit to the chosen measurement, or a custom value (blender/blender@0c544974d1690d8476031b9c6965f1920fb90494)

## glTF

- Importer
  - Features
    - Add option to import scene extras or not
      (blender/blender@dbb670c6d1d9f724cd8bfd447cc90a7543a79f87)
    - Add option to not select created objects
      (blender/blender@599788666278d0ce5a822c02ebf2be316a9015df)
  - Fixes


- Exporter
  - Features
    - Always bake scene animation, so driven animated properties can be exported
      (blender/blender@7ea11a47242bcb284e660d8b7ce91e7f40cb394c)
  - Fixes
    - Fix some sanity poll checks
      (blender/blender@b6346312c4751566a456d295e10009189abd3881)
    - Fix crash when 'remove armature object' + 'export only def bones'
      (blender/blender@14302f6af7e5198c8f12d908797fd67e643f311c)
    - Fix crash with extra channel
      (blender/blender@90e09b93b54527c34272aff292595607857e30f0)
    - Fix UI (disable option when not available)
      (blender/blender@f8766c5542452158d209767baa43f3b5a489ad6f)
    - Fix armature object keep channel option
      (blender/blender@ba737a496c73cf1f5249f4d8ef74799d3c99531e)
    - Fix debug logging
      (blender/blender@f7a2d0095e8040b7a2ddc4e6df637aeca2f5b408)
    - Fix crash when animation on not used material
      (blender/blender@428ec6c1e3beadfb924eb15aa32f22f35bae1f69)
    - Fix KHR_animation_pointer to use the material material when texCoord
      (blender/blender@373191ff2adf4a32c4c73607841e813ed6313209)
