# Blender 4.4: User Interface

## Asset Browser

- New option to sort assets by asset catalog instead of by name.
  This is the new default sorting method.
  (blender/blender@f49d0390a1b6)
- New operator to remove the preview of an asset. Find it in the Asset Browser sidebar. (blender/blender@40b0185a8e)

## General

- Overlays: Mesh indices overlay setting is now always visible, it no longer depends on Developer Extras. (blender/blender@ef2bff2004)
- Tree-views no longer forget their changed height after hiding/unhiding them,
  or reloading the file (if _Load UI_ is enabled).
  (blender/blender@f0db870822)
- Improved container/codec ordering in FFMPEG video drop-downs. (blender/blender!118412)
- Custom integer properties no longer have -10000/10000 soft minimum and maximum.
  (blender/blender@9e6c884394)
- Brush names are easier to read with Light theme. (blender/blender@cec1d5f68)
- Notification shown when hiding objects. (blender/blender@bc5fcbe1c3)
- Opening context menu from numerical input now shows correct mouse cursor.
  (blender/blender@e6d941cdf4)
- Tool-tips have improved vertical centering. (blender/blender@8dc8e48e9a)
- Can not un-isolate collection when a linked collection is present.
  (blender/blender@f181262634)
- Improved Outliner vertex group sorting. (blender/blender@531ed68061)
- Scroll-bar properly hidden for tiny areas. (blender/blender@b846797074)
- Full file name now shown on the Recent items tool-tips. (blender/blender@0e83b9c5ee)
- Improved calculation of text string length for monospaced fonts. (blender/blender@7492c7aa6)
- Centered dialogs can now be moved without them snapping back. (blender/blender@154ab16ee2)
- Auto keyframe toggle now works better if assigned to keyboard shortcut. (blender/blender@0e1313483a)
- Can delete "Measure" tool items when gizmos are turned off. (blender/blender@3f91350d4e)
- Tooltip status colors work better on light backgrounds. (blender/blender@c5bce9d710)
- Outliner supports ctrl/shift for excluding collections. (blender/blender@a1fc2eb37a)
- Preference for UI Icon alpha no longer affects matcaps and studio lights. (blender/blender@2b7a968909)
- Improved tab key navigation between inputs in the color picker. (blender/blender@93e8c231aa)
- Using Text Objects with a non-default font, missing characters are no longer loaded from other fonts. (blender/blender@6fa5295c1d)
- Using Text Objects with a missing or invalid font, characters no longer used from default font. (blender/blender@5ce10722ab08)
- Can now play animation while in Sculpt mode. (blender/blender@1debbcae1e)
- Language translation options are now preserved when changing languages. (blender/blender@b801615431)
- Popup informational alerts, like Python script warning, adjust width based on contents. (blender/blender@0add2857a3)
- Status Bar notification banners are now truncated when very long. (blender/blender@0ba91e9870)
- Improved selection from Fonts folder. (blender/blender@7a1e95ea91)
- No more overlapping icons in Outliner with some display options. (blender/blender@25febbbb32)
- UI Lists now be sorted in reversed alphabetical. (blender/blender@72a9990779)
- Correct shortcuts now shown when hovering over Editor Change button. (blender/blender@9ed7b03e35)
- Color tooltips now propertly show colors that are without alpha. (blender/blender@dbe50711ce)
- Tooltips can now show while animation is playing. (blender/blender@a7334db09b)
- Improved feedback during Animation Playback timer test. (blender/blender@df57beb676)
- More user preferences now reset to actual defaults. (blender/blender@6a06e87f85)
- Tree View displays lines correctly when local zoom changes. (blender/blender@5ec2547ab0)
- Improved current frame indicator styling in movie clip and image editors. (blender/blender!132911)
- _Adjust Last Operation_ becomes unavailable after undoable interactions with UI elements. (blender/blender@1bde901bf28e)

## Viewport

- Default color for front face for Face Orientation overlay changed to transparent.
  (blender/blender@8bcb714b9e)

## Area Maintenance

- Resizing areas now softly snaps to minimum and maximum.
  (blender/blender@62541bffc2)
- Improved rounded edge for splitting preview.
  (blender/blender@c3ea941273)
- Docking operation feedback descriptions improved.
  (blender/blender@09e6bab7bc)
- Minimum size imposed for docking operation creation of new area.
  (blender/blender@f339ab4325)
- Splitting improvements including soft snapping and minimum size increased.
  (blender/blender@82667d5626)
- Using separate specific mouse cursors for Join operations.
  (blender/blender@798a48f6ef)

## Spreadsheet

- The selection filter for meshes now gives expected results for non-vertex domains.
  (blender/blender@76e9dc0f0424)

## Nodes

- The sidebar is see-through when *Region Overlap* is enabled in the Preferences, similar to the 3D Viewport (blender/blender@9b7e660fad).
- Panels in node groups can now be nested (blender/blender@7dc630069b48fa16ce1d4aedc1513dd5bdd6cdb7).
- The name input in the Attribute node in the shader editor is wider now (blender/blender@ea8c45e10a154be22a9f7d47f5cbfb49ab368f84).
- Node title text truncation is improved (blender/blender@5edc68c442, blender/blender@4ff47d85e0).

## Preferences

- Improved Studio Lights Editor interface layout.
  (blender/blender@f6bbcaba6d)
  
![Studio Lights Editor Layout](images/studio_lights_editor_layout.png)

## Platforms

- Windows: You can now copy and paste OS image paths into Image Editor.
  (blender/blender@0193579e90)
- Windows: File system volume names display correctly with high-bit Unicode characters.
  (blender/blender@fe6609c4eb)
- macOS: Fixed title bar file icon not being cleared when creating a new file.
  (blender/blender@b99497bf21)
- Linux: Fixed text pasting from Blender to certain other applications (such as Firefox) not working under X11.
  (blender/blender@720fc44ca38330bbc1b8cfec5f37b56a2d6b46c3)
  
## Animation Editors

- Animated values for nodes, node sockets, and geometry nodes inputs have more useful names in the list (blender/blender@bd8edc7c27513842b083fc0a481a4b9935dd13a6).
