# Blender 4.4: Compositor

## Added

- Integer sockets are now supported in the compositor. (blender/blender@b3623feab2)
- Exposed the Quality option of OpenImageDenoise on the Denoise Node. (blender/blender@8efd41271d)
  - This allows users to make trade offs between quality and performance of denoising in the
    compositor, similar to what can already be done for Cycles renders.

## Changed

- The Glare node was revamped to provide a better user experience and more flexible control:
  - Node options are now single value inputs that can be linked. (blender/blender@004e3d39fa)
  - The generated glare and the highlights are now exposed as outputs. (blender/blender@004e3d39fa)
  - A new Strength input now controls the strength of the glare, and can boost the glare power.
    (blender/blender@004e3d39fa)
  - Fog Glow size is now linear, relative to the image size, and can have any size.
    (blender/blender@004e3d39fa)
  - Bloom size is now linear and relative to the image size. (blender/blender@004e3d39fa)
  - Bloom now conserves energy better and has a more reasonable output range.
    (blender/blender@b92a6eab3a)
  - The saturation and tint of the glare can now controlled using the new Saturation and Tint
    single value inputs. (blender/blender@c37d51f73d)
  - The highlights can now be clamped and smoothed using the new Smoothness and Maximum single
    value inputs. (blender/blender@3b28cf276e)
  - The inputs are now organized into panels for compactness and clarity.
    (blender/blender@14a380089a)

![The new Glare node.](images/newGlareNode.png)

## Compatibility

- The Glare node options were turned into inputs, are now deprecated, and will be removed in a
  future version.
- The Fog Glow and Bloom modes of the Glare node were corrected due to the revamped Glare node, but
  the input image size was assumed to be the render size during correction, since guessing the real
  input size was difficult. If this was not the case, turning the Size and Strength inputs should
  restore the old result.
