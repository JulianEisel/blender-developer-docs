# Blender 2.90: Import & Export

## Alembic

- Matrices with negative scale can now be properly interpolated. This
  means that it is now possible to take an Alembic file that was saved
  at 30 FPS and load it into a 24 FPS Blender file, even when there are
  mirrored objects.
  (blender/blender@a5e176a8ed8a
  and demo videos in [D8048](https://developer.blender.org/D8048)).
- Object data is now exported with the object data name
  (blender/blender@0d744cf673e).
  Previously the Alembic exporter exported a mesh object to
  `{object.name}/{object.name}Shape`. Now it exports to
  `{object.name}/{mesh.name}` instead. The same change also applies to
  other object data types.
- Spaces, periods, and colons in the names of particle systems are now
  replaced with underscores
  (blender/blender@f106369ce85).
  Other types already had spaces, periods, and colons replaced by
  underscores, and now particle systems are exported with the same
  naming convention.
- Blender now always exports transforms as as "inheriting", as Blender
  has no concept of parenting without inheriting the transform
  (blender/blender@2dff08c8ce9).
  Previously only objects with an actual parent were marked as
  "inheriting", and parentless objects as "non-inheriting". However,
  certain packages (for example USD's Alembic plugin) are incompatible
  with non-inheriting transforms and will completely ignore such
  transforms, placing all such objects at the world origin.
- Blender now exports Alembic using the same approach as USD. This means
  that Alembic and USD files are more consistent, and that it's easier
  to solve (or even prevent) certain bugs
  (blender/blender@2917df21adc).
  Changes are:
  - Duplicated objects now have a unique numerical suffix (also see
    above).
  - Matrices are computed differently. This fixes T71395, but otherwise
    should produce the same result as before (but with simpler, more
    predictable code).
- Alembic's obsolete HDF5 compression format has never been officially
  supported by Blender, but only existed as an optional build-time
  option that was disabled for all Blender releases. The support for
  HDF5 has now been completely removed
  (blender/blender@0c3843622726,
  blender/blender@0102b9d47edf).

## USD

- Hair particle systems are now exported using the Particle System name.
  Previously this was done with the Particle Settings name. This gives
  more control over the final name used in USD and is consistent with
  the Alembic exporter.
  (blender/blender@fc0842593f0).

## Instances

USD and Alembic export can now correctly export nested instances.
Further, numbers used to identify duplicated (i.e. instanced) objects
have been reversed
(blender/blender@98bee41c8a3f78c).

This produces hierarchies like this:

`     Triangle`  
`       |--TriangleMesh`  
`       |--Empty-1`  
`       |    +--Pole-1-0`  
`       |        |--Pole`  
`       |        +--Block-1-1`  
`       |            +--Block`  
`       |--Empty`  
`       |    +--Pole-0`  
`       |        |--Pole`  
`       |        +--Block-1`  
`       |            +--Block`  
`       |--Empty-2`  
`       |    +--Pole-2-0`  
`       |        |--Pole`  
`       |        +--Block-2-1`  
`       |            +--Block`  
`       +--Empty-0`  
`           +--Pole-0-0`  
`               |--Pole`  
`               +--Block-0-1`  
`                   +--Block`

It is now clearer that `Pole-2-0` and `Block-2-1` are instanced by
`Empty-2`. Before Blender 2.90 they would have been named `Pole-0-2`
and `Block-1-2`.

## glTF 2.0

- Importer
  - Enhancements
    - Implement KHR_materials_clearcoat
      (blender/blender-addons@64d34396670d)
    - Implement KHR_mesh_quantization
      (blender/blender-addons@4b656b65f81d)
    - Improve the layout of nodes in the material graph
      (blender/blender-addons@5b4ed4e574ab)
    - Add option to glue pieces of a mesh together
      (blender/blender-addons@c7eda7cb49f7)
    - Code cleanup & refactoring & performance
      (blender/blender-addons@bb4dc6f1daab,
      blender/blender-addons@9fd05ef46664)
  - Fixes
    - Fix occlusion textures
      (blender/blender-addons@a29e15e11ed2)
    - Fix Crash on undo after glTF import
      (blender/blender-addons@d777821fd6ad)
- Exporter
  - Enhancements
    - Add joint / pre /post in hook for extensions
      (blender/blender-addons@e3bb132d1f68,
      blender/blender-addons@40db41a902be)
    - Allow combining different-sized textures (eg for ORM)
      (blender/blender-addons@09508f2dcf2a)
    - Add check rotation + delta rotation both animated
      (blender/blender-addons@ee2a0831d8c1)
    - Code clean up & refactoring & performance
      (blender/blender-addons@2d8c1b2c6184,
      blender/blender-addons@01186b0df9c5,
      blender/blender-addons@3ea1673580ab,
      blender/blender-addons@9313b3a155bb,
      blender/blender-addons@03e3ef7f71f2)
    - Refactoring Normals export
      (blender/blender-addons@bd8e1f3e576f,
      blender/blender-addons@422c47c5f79e,
      blender/blender-addons@52f88967a6e7)
    - Add support for use_inherit_rotation and inherit_scale
      (blender/blender-addons@63dd8498ac10)
    - Export curve/surface/text objects as meshes
      (blender/blender-addons@47ea656bdd61)
  - Fixes
    - Prevent infinite recursion when mesh is skinned and parenting to
      same bone
      (blender/blender-addons@72227fc13ba3)
    - Make sure rotation are normalized
      (blender/blender-addons@fac4c6443ba7)
    - Add check when armature animation is binded to mesh object
      (blender/blender-addons@61f7f5f3a57b)
    - Fix extension panel appearance
      (blender/blender-addons@aea05413b768)
    - Fix draco UV export
      (blender/blender-addons@e47d2bcfad93)
    - Fix lamp parented to bone
      (blender/blender-addons@1d29fc5b9161)
    - Fix saving use_selection option in .blend file
      (blender/blender-addons@7cbb383d2213)
    - Fix exporting with option "group by NLA"
      (blender/blender-addons@7a0a9182c82b)
    - Fix exporting EXTEND textures
      (blender/blender-addons@cbad9300d7e1)
    - Fix export alpha scalar value (not coming from texture)
      (blender/blender-addons@48c8d6c23010)
    - Fix exporting `aspectRatio` for Perspective Cameras
      (blender/blender-addons@2b4bf943d0a3)
    - Fix to generate valid file when zero-weight verts
      (blender/blender-addons@386bb5eaa473)
