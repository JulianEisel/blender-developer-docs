# Blender 4.1 Release Notes

Blender 4.1 was released on March 26, 2024.

Check out out the final [release notes on
blender.org](https://www.blender.org/download/releases/4-1/).

* [Animation & Rigging](animation_rigging/index.md)
* [Compositor](compositor.md)
* [Cycles](cycles.md)
* [EEVEE](eevee.md)
* [Geometry Nodes](nodes_physics.md)
* [Import & Export](pipeline_assets_io.md)
* [Modeling](modeling.md)
* [Python API](python_api.md)
* [Rendering](rendering.md)
* [Sculpting](sculpt.md)
* [Sequencer](sequencer.md)
* [User Interface](user_interface.md)
* [Add-ons](add_ons.md)

## Compatibility

* Libraries have been upgraded to match [VFX platform 2024](https://vfxplatform.com/), including:
  * Python 3.11
  * OpenColorIO 2.3
  * OpenEXR 3.2
  * OpenVDB 11.0
  * OpenShadingLanguage 1.13
  * USD 23.11

* macOS 11.2 (Big Sur) is now the minimum required version for Apple computers.

* Intel Arc is supported with driver version `101.5186` on Windows and `XX.XX.27642.38` on Linux.

## [Corrective Releases](corrective_releases.md)
