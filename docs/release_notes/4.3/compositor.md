# Blender 4.3: Compositor

## Added

- Multi-pass compositing support for *EEVEE* was added. (blender/blender@57ff2969b8)

<figure>
<video src="../videos/multiPassViewportCompositingExample.mp4"
       title="Multi-pass Viewport Compositing Example"
       controls=""></video>
<figcaption><a href="https://studio.blender.org/characters/rain/" target="_blank">Rain</a> character by Blender Studio. Compositing by Pau Homs.</figcaption>
</figure>

- A *White Point* conversion mode was added to the *Color Balance* node, which works similarly to
  the *White Balance* view transform but also allows control over the target white point.
  (blender/blender@021bce8b48)
- A new global *Save As Render* option was added to the File Output node to be used if
  *Use Node Format* is enabled. (blender/blender@9c44349204)
- The GPU compositor now supports Cryptomatte meta-data necessary for exchanging Cryptomatte through
  EXRs using the *File Output* node. (blender/blender@f954a6b5fb)

## Removed

- The *Auto Render* option is now removed. (blender/blender@cbabe2d3ef)
