# Blender 4.3: EEVEE & Viewport

## Light & Shadow Linking

There is now feature parity with Cycles.

With light linking, lights can be set to affect only specific objects in the scene.
Shadow linking additionally gives control over which objects act as shadow blockers for a light
(blender/blender@5a2728091647fe16f1067743da56533edc07c40e).

This adds greater artistic control over lighting by breaking the traditional laws of physics. For example, the environment and characters in a shot can have different lighting setups. A character might have a dedicated linked rim light to help it stand out, while shadow linking can ensure that objects in the environment don't block the light.

See [Light Linking](https://docs.blender.org/manual/en/4.3/render/lights/light_linking.html) in the user manual for more details.

<figure>
<video src="../videos/eevee_light_linking.mp4"
       title="EEVEE Light Linking"
       controls=""></video>
<figcaption>Lighting Linking example using the <a href="https://www.blender.org/download/demo-files/#eevee" target="_blank">Tree Creature</a> file by Daniel Bystedt.</figcaption>
</figure>

## User Interface
- Add toggle for Fast GI Approximation, similar to Cycles (blender/blender@000d34c398).

## Vulkan Backend (experimental)

On Windows and Linux, it is possible to use [Vulkan](https://www.vulkan.org/) to render the user
interface. This is still an experimental feature. The goal of this release is to gather feedback
on compatibility and allow add-on developers using the `gpu` module to test their add-ons.

For testing, go to **Preferences** and enable **Developer Extras** in the **Interface** section.
Then, in the **System** section change the **Backend** from **OpenGL** to **Vulkan (experimental)**.
After restarting Blender, this section also has a new **Device** option, that allows selecting a preferred GPU.

### Hardware Support

#### Windows

- **NVIDIA**: GTX900 and up are supported using the latest official drivers from NVIDIA.
- **AMD**: RX 400 series and up are supported using the latest official drivers from AMD.
- **Intel**: Intel iGPU 11th gen and above and Arc based GPUs are supported using the latest drivers from Intel.

NOTE: Intel CPUs 10th gen and older and Legacy AMD GPUs are not supported at this time. Support will be added in a future release. Depth picking is disabled for NVIDIA/AMD.

#### Linux

- **NVIDIA**: GTX900 and up are supported using the latest official drivers from NVIDIA.
- **AMD**: RX 400 series and up are supported using the latest official drivers from AMD.
  Legacy AMD GPUs are supported using Mesa drivers.
- **Intel**: Intel UHD, IRIS and Arc based GPUs should be supported using Mesa drivers.

NOTE: Depth picking is disabled for NVIDIA.

### Limitations

- GPU subdivision is not supported.
- OpenXR is not supported.
- Performance can be slower compared to OpenGL. This release targets feature parity and stability.

### Known Issues

- Particle hair doesn't work with EEVEE
- Color scopes have artifacts
- UI on NVIDIA GPUs has white artifacts

## Other

- Objects with volume probe visibility turned off now cast shadow properly during lightprobe volume baking
  (blender/blender@bc3fdc329378128dc4253681f869e01d924da213).
