# Animation & Rigging

## Dopesheet & Timeline: Grease Pencil in main modes

The Timeline and Dopesheet editors now show Grease Pencil keyframes,
without having to switch to Grease Pencil mode
(blender/blender@92d7f9ac56e0).

## Dopesheet Editor: Custom Properties for Actions

![](../../images/blender-33-dopesheet-custom-props.png)

The Action mode of the Dopesheet now shows the active Action's custom
properties
(blender/blender@57816a6435c3).
Before, these were only accessible via Python.

## NLA: Frame Start / End sliders

The NLA's Strip Frame Start / End sliders now behave more predictable
(blender/blender@bd00324c264c).
Manipulating the Frame Start slider will just slide the entire strip
(instead of changing its playback speed to make it longer/shorter). The
Frame End slider behavior depends on the Repeat setting:

- 1 Repeat: the underlying Action is made longer/shorter.
- Less or more than 1 repeat: the number of repeats is adjusted.

The Frame Start / End sliders no longer stretch the timing of the strip.
This can be done explicitly, as before, with the Animate Time settings.

## Python API

- `action.fcurves.clear()` can now be used to remove all FCurves from
  the given Action
  (blender/blender@091100bfd757).
- `fcurve.keyframe_points.clear()` can be used to delete all keyframe
  points of an FCurve
  (blender/blender@4812eda3c5d1).
- [NLAStrip](https://docs.blender.org/api/master/bpy.types.NlaStrip.html)
  has two new properties: `frame_start_ui` and `frame_end_ui`
  (blender/blender@bd00324c264c).
  These behave like described above. The already-existing properties
  `frame_start` and `frame_end` have been simplified, and just
  ensure data consistency now (i.e. no overlapping NLA strips).
