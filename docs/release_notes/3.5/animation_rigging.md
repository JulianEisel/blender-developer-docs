# Animation & Rigging

## General

- The old (pre-3.0) pose library has been removed
  (blender/blender@48b5dcdbe857).
  They are now seen by Blender as simply Actions with named markers. It
  is recommended to [convert them to pose
  assets](https://docs.blender.org/manual/en/3.5/animation/armatures/posing/editing/pose_library.html#converting-old-pose-libraries).
- Fixed an old bug where the effect of the Invert Vertex Group toggle of
  the Armature modifier was inverted when Multi-Modifier was active. Old
  tutorials explaining the usage of the Multi-Modifier option will need
  updating.
  (blender/blender@ea1c31a24438)
- Added the pin icon to the Dope Sheet to pin channels.
  (blender/blender@49ad91b5ab7b)
- Added "Select Linked Vertices" to weight paint mode.
  (blender/blender@04aab7d51620)
- Take subframes into account when jumping to next/previous keyframes.
  (blender/blender@5eab813fc078)
- Motion paths have a new frame range option "Manual Range" to ensure
  the frame range is never changed on update
  (blender/blender@d72c7eefd1c5).
  In other modes ("All Keys", "Selected Keys", "Scene Frame Range") the
  start/end frame are still editable, but greyed out.
- Adding F-Curve modifiers to multiple channels at once is now easier to
  access
  (blender/blender@0f51b5b599bb).

![Motion Paths frame range options](../../images/Blender-3.5-anim-motion-paths.webp){style="width:600px;"}

## Asset Browser and Pose Library

The Pose Library went through a few usability changes to unify the
experience of the other asset types
(blender/blender-addons@c164c5d86655).

![](../../images/Poselib_assetbrowser.png){style="width:800px;"}

The Pose Library functionality in the asset browser were moved to a new
Asset menu, as well as the pose asset context menu.

![](../../images/Poselib_contextmenu.png)

In the viewport, a few options were removed:

- The option to Create Pose Assets is no longer there - use instead the
  Action Editor or the Asset Browser.
- The Flip Pose check-box is gone - flipped poses can be applied
  directly via the context menu. When blending, keep CTRL pressed to
  blend the flipped pose.
- The `poselib.apply_pose_asset_for_keymap` and
  `poselib.blend_pose_asset_for_keymap` operators are gone. If you
  have assigned these in your keymap, use the regular apply/blend
  operators (`poselib.blend_pose_asset` and
  `poselib.apply_pose_asset`) instead.

Other improvements are:

- A pose asset can now be "subtracted" while blending. Drag to the right
  to blend as usual, **drag to the left to subtract the pose**.
- While blending, keep Ctrl pressed to flip the pose
  (blender/blender@bd36c712b928f522).
- Blending can now also exaggerate a pose, by pressing E (for
  Extrapolate) and applying a pose for more than 100%
  blender/blender@74c4977aeaa3.

<video src="../../../videos/Blender-3.5-poselib-bidirectional-blending.mp4" controls=""></video>

## Graph Editor

#### Ease Operator

The Graph Editor got a new Operator, "Ease"
(blender/blender@76a68649c1c1),
that can align keys on an exponential curve. This is useful for quickly
making an easing transition on multiple keys. It can be found under
<span class="literal">Key</span> » <span class="literal">Slider
Operators</span>.

<video src="../../../videos/Graph_Editor_Ease_Operator.mp4" width="1280" controls=""></video>

## Pose Mode

#### Propagate Pose

The Propagate Pose Operator has been updated
(blender/blender@200a114e1596).
Previously it used to evaluate each FCurve individually to find the
target key. That resulted in undesired behavior when the keys where not
on the same frame. Now it finds the frame first by checking all FCurves
in question and then propagates the pose to it. It will add a keyframe
to it in case it doesn't exist. The discussion can be found here
<https://developer.blender.org/T87548>

<video src="../../../videos/Pose_Propagate_Demo.mp4" width="1280" controls=""></video>
