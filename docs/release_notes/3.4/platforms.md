## Linux

- The Wayland windowing environment is now supported in addition to X11,
  see the
  [user-manual](https://docs.blender.org/manual/en/3.4/getting_started/installing/linux_windowing_environment.html)
  & this
  [blog-post](https://code.blender.org/2022/10/wayland-support-on-linux)
  for details.

## Windows

- Allow Windows Explorer thumbnails of Blend files at maximum
  resolution.
  (blender/blender@ff27b68f41ce).
