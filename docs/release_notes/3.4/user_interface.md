# User Interface

## Outliner

- Searching by name isn't possible anymore in the Hierarchies view of
  the Library Overrides display mode. This is to workaround big
  performance issues in complex scenes.
  (blender/blender@21b92a5f31a4)
- The data-block type filter was removed from the Properties view of the
  Library Overrides display mode. It isn't needed anymore since
  data-blocks are now grouped by type anyway.
  (blender/blender@b5fc8f611e39)
- The context menu doesn't show whole sections of operations that cannot
  operate on the selected elements anymore.
  (blender/blender@7eda9d8dda59)
- Improved element count display with better readability and higher
  numbers.(blender/blender@a5d3b648e3e2,
  blender/blender@84825e4ed2e0).

|Before|After|
|-|-|
|![](../../images/D16284_-_before.png)|![](../../images/D16284_-_after.png)|

## Viewport

- The select menu (accessed by holding Alt while picking objects &
  bones) now orders results by distance (nearest first)
  (blender/blender@47d3e765679e54c09ecc4ded812e89483a1e1d9b).
- Improved display of text caret and selection when editing 3D Text
  Objects
  (blender/blender@a1e01f4c026a).

![](../../images/3DCaret.png){style="width:600px;"}

## General

- Added shortcuts
  <span class="hotkeybg"><span class="hotkey">PageUp</span></span> and
  <span class="hotkeybg"><span class="hotkey">PageDown</span></span> to
  the console to scroll entire pages, as well as
  <span class="hotkeybg"><span class="hotkey">↖ Home</span></span> to
  reset scrolling to the bottom.
  (blender/blender@82fc52ffc881)
- Detect existing file and add +/- auto-increase for output filepaths
  (render filepath and File Output node for now)
  (blender/blender@78bfaf1a4fe1).

![](../../images/Auto-increase.png){style="width:600px;"}

- Font thumbnails now preview languages, shapes, content, and intended
  use better.
  (blender/blender@4e7983e07320).

![](../../images/FontThumbs.png){style="width:600px;"}

- Improved editing of text that contains non-precomposed diacritical
  marks.
  (blender/blender@12fdf9069abe).
