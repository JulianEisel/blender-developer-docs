# Blender 4.2 LTS: Extensions

Extensions are add-ons, themes, and potentially anything else that can extend Blender’s native functionality (e.g. key maps, assets). An extension is an archive (.zip) containing the files and a manifest with metadata such as license, copyright, permissions, useful links, etc.

- [Announcement](https://code.blender.org/2024/05/extensions-platform-beta-release/).
- [User manual](https://docs.blender.org/manual/en/4.2/advanced/extensions/getting_started.html).
- Initial commit (blender/blender@c4a0bbb1f4d5e9496bc79ed767a58c156a49820d).

## Bundling

When working offline or in a more controlled environment, it may be useful to provide a set of extensions to all users. These can be served from the [default read-only System repository](https://docs.blender.org/manual/en/4.2/advanced/deploying_blender.html#bundling-extensions).