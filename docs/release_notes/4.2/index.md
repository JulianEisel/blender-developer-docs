# Blender 4.2 LTS Release Notes

Blender 4.2 LTS was released on July 16, 2024.

It will be maintained until July 2026.

Check out out the final [release notes on
blender.org](https://www.blender.org/download/releases/4-2/).

<!---
This release includes long-term support, see the [LTS
page](https://www.blender.org/download/lts/4-2/) for a list of bugfixes
included in the latest version.
-->

* [Animation & Rigging](animation_rigging.md)
* [Color Management](rendering.md)
* [Compositor](compositor.md)
* [Core](core.md)
* [Cycles](cycles.md)
* [EEVEE](eevee.md)
* [Extensions](extensions.md)
* [Geometry Nodes](geometry_nodes.md)
* [Import & Export](pipeline_assets_io.md)
* [Modeling & UV](modeling.md)
* [Physics](physics.md)
* [Python API](python_api.md)
* [Sculpt](sculpt.md)
* [User Interface](user_interface.md)
* [Video Sequencer](sequencer.md)

---

## Compatibility
### Hardware Requirements
On Windows and Linux a CPU with **SSE4.2** is now required.
This is supported since AMD Bulldozer (2011) and Intel Nehalem (2008).

[See all compatibility changes](../../release_notes/compatibility/index.md)

### Compositor
Due to added support for GPU rendering and CPU optimizations, some nodes have changed behavior, and certain compositing setups will require manual adjustments. [See all breaking changes](compositor.md#breaking-changes).

### Add-ons
Most add-ons that used to ship with Blender, are now available on the [Extensions Platform](https://extensions.blender.org/) where you can browse, install, and update them online from within Blender. **Read more about [Extensions](extensions.md)**.

Add-ons that haven't been converted to extensions yet, are still supported as [legacy add-ons](https://docs.blender.org/manual/en/4.2/extensions/addons.html#legacy-vs-extension-add-ons) and can be [installed from disk](https://docs.blender.org/manual/en/4.2/editors/preferences/extensions.html#install) as usual.

To ease the distribution and transition to the new system, a bundle of the add-ons that shipped with Blender 4.1 is available for [download](https://www.blender.org/download/release/Blender4.2/add-ons-legacy-bundle.zip).

### IDProperties

System-defined and library-overridable IDProperties [are now statically typed](python_api.md#statically-typed-idproperties).
