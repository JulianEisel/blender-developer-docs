# Blender 4.2 LTS: Sequencer

## Performance

- Reduced stalls when new movie clips start playing by caching FFmpeg RGB<->YUV conversion contexts
  (blender/blender@ffbc90874b4f) and reducing amount of redundant work that is done for FFmpeg
  initialization
  (blender/blender@b261654a931).
- Reduced amount of temporary image clears when rendering VSE effect stack
  (blender/blender@b4c6c69632)
- VSE already had an optimization where an Alpha Over strip that is known to be fully opaque and
  covers the whole screen, stops processing of all strips below it
  (since they would not be visible anyway).
  Now the same optimization happens for some cases of strips that do not cover the whole screen:
  when a fully opaque strip completely covers some strip that is under it,
  the lower strip is not evaluated/rendered.
  (blender/blender@f4f708a54f9f)

## User Interface

- Strips in the timeline received a visual overhaul:
  - Rounded corners to better tell where a strip begins and ends. (blender/blender!122576)
  - Thicker outline for active/selected strips, including an additional outer and inner dark edge to provide contrast.
  - Theme: Strip colors have been updated to improve readability (blender/blender@abb233dd1e)
  ![](images/VSERoundedCorners.png)
  - Strips with missing media files are displayed with red tint and an icon overlay in the VSE timeline. (blender/blender!116869)
  ![](images/VSEMissingMedia.png)
- Tweaking of handles was streamlined (blender/blender@f98b01e49):
  - Unselected handles are not drawn, but mouse cursor will change when hovering over handle.
  - Adjoined handles can be selected with one click.
  - After moving unselected handle, the handle will be deselected.
  - This behavior can be disabled in preferences - Editing > Video Sequencer > Tweak Handles.
- Overlays: New cache line overlay (previously only available when using "Developer Extras") (blender/blender@221951657b)
- Overlays: Waveforms are now displayed as half-size by default, for space efficiency (blender/blender@a4dce75123)
- Overlays: The popover has been reorganized for clarity. (blender/blender@6e42c3d920)
- Preview: "Separate colors" option for Luma waveform was replaced by "RGB Parade" scope. (blender/blender@740c9f220d)
- Text Strips: New options for shadow placement and blur level, as well as text outline. (blender/blender!121478)
  ![](images/VSETextOptions.png)
- Multiple files from external file browser can be added at once using drag and drop. (blender/blender@89c1f7e0c3)
- New operator in the View (regular + pie) menu: "Frame Scene Range" (blender/blender@95966813ae58).
  This changes the horizontal view to match the scene range. If the [preview range][preview-range]
  is active, the operator will be re-labeled as "Frame Preview Range" and zoom to that instead. This
  was added to the Dope Sheet, Timeline, Graph Editor, NLA Editor, and Video Sequence Editor.

  [preview-range]: https://docs.blender.org/manual/en/4.2/editors/timeline.html#frame-controls

## Video

- "AVI RAW" and "AVI JPEG" movie file output types are removed.
  Existing scenes using them will be updated to "FFmpeg Video" type with default options (H.264).
  (blender/blender@f09c7dc4ba58)
- Redundant timecode types were removed. "Record Run" and "Record Run No Gaps" are kept. (blender/blender@69472c88ee)
