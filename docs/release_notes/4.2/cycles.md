# Blender 4.2 LTS: Cycles

## Ray Portals

The Ray Portal BSDF transports rays to another location in the scene, with specified ray position and normal. It can be used to render portals for visual effects, and other production rendering tricks. (blender/blender!114386)

See the [documentation](https://docs.blender.org/manual/en/4.2/render/shader_nodes/shader/ray_portal.html) for details and example node setups.

![Connecting two spaces through a portal](images/render_shader-nodes_ray-portal-bsdf_gateway-example.jpg)
![Simulating a camera feed](images/render_shader-nodes_ray-portal-bsdf_portal-to-screen-example.jpg)

## Thin Film Interference

The Principled BSDF now supports physically accurate thin film interference effects for specular reflection and transmission. (blender/blender!118477)

Initially, this is only applied to dielectric materials, support for metallic thin film effects is planned later. Similarly, the effect is not yet supported by EEVEE.

![Simulating soap bubbles](images/thinfilm-bubbles.jpg)

## Shaders

* Principled Hair with the Huang model now provides more accurate results when viewing hairs close up, by dynamically switching between a near and far field model depending on the distance. (blender/blender!116094)
* Subsurface Scattering node now has a Roughness input, matching the Principled BSDF. (blender/blender!114499)

## Denoising

* Upgrade OpenImageDenoise to version 2.3, with improved denoising quality.
* GPU acceleration is now enabled on AMD GPUs on Windows and Linux.
* CPU renders can now also use GPU accelerated denoising. (blender/blender!118841)<br/>
  The denoiser uses GPU device configured in the User Preferences. The side-effect of this is that
  OptiX denoiser requires having OptiX compute device configured in the preferences.

## Sampling

* Blue noise dithered sampling to improve the visual quality of renders. This mainly helps when setting the maximum number of samples to a low number, and for interactive viewport renders. New files use this mode by default, while existing files can be changed in the Sampling → Advanced panel. (blender/blender!123274)

| |Classic|Blue Noise|
|-|-|-|
|Raw|![](images/noise_white.jpg)|![](images/noise_blue.jpg)|
|Denoised|![](images/noise_white_denoised.jpg)|![](images/noise_blue_denoised.jpg)|

* Improved volume light sampling, particularly for spot lights and area light spread. (blender/blender!119438) (blender/blender!122667)

## Other

* World Override option for view layers. (blender/blender!117920)
* Intel GPU rendering now supports host memory fallback. (blender/blender!122385)<br/>
Drivers 101.5518-101.5525 and 101.5768 and higher are currently recommended for this feature. (blender/blender#124763)
* GPU kernels are now compressed, reducing Blender installation size. (blender/blender!123557)
* Motion Blur settings are now shared with EEVEE. (blender/blender@74b8f99b43)
* Embree has been updated to 4.3.2 (blender/blender!122242) which relies on more driver components to provide Hardware Ray-tracing:<br/>
Intel GPU users on Linux should ensure [intel-level-zero-gpu-raytracing](https://github.com/intel/level-zero-raytracing-support/releases) package is installed as mentioned in the [documentation](https://dgpu-docs.intel.com/driver/client/overview.html).