## Library Overrides

- Added limited support for simulation caches on override data-blocks
  (caches need to be written on disk for now,
  blender/blender@59910f72,
  blender/blender@7e210e68,
  blender/blender@50ccf346).
- Added initial support for NLA on override data-blocks (one can add new
  tracks to existing NLA system, and edit some properties of the
  existing tracks and strips
  (blender/blender@c0bd240ad0a1).
- Some heavy data from override IDs do not get written to disk in the
  local .blend file anymore (mainly mesh geometry, shapekeys data, and
  embedded files currently, see
  [D9810](http://developer.blender.org/D9810),
  blender/blender@f5a019ed43ab).

## glTF 2.0

### Importer

- Importer can now open Draco encoded files
  (blender/blender-addons@eb29a12da48e)
- Implement occlusion strength
  (blender/blender-addons@d5c0d4b77c15)
- Show specific error when buffer resource is missing
  (blender/blender-addons@285deaf22c27)
- Removed future deprecated encoding arg to json.loads
  (blender/blender-addons@48ec56bde5fc)
- Create placeholder images for files that can't be loaded
  (blender/blender-addons@59f60554f18c)
- Make NLA track order match glTF animation order
  (blender/blender-addons@d7f8d9a4f919)
- Use Separate R node for clearcoat textures
  (blender/blender-addons@ebdf1861dc56)

### Exporter

- Fix export when texture is used only for alpha
  (blender/blender-addons@5f2cb885abb9)
- Take care of active output node
  (blender/blender-addons@5088c8d9d735)
- Implement occlusion strength
  (blender/blender-addons@d5c0d4b77c15)
- Avoid crash when background exporting (without UI)
  (blender/blender-addons@c5a84e2e1ad0)
- Fix texture filtering
  (blender/blender-addons@9ff0d983ee49)
- Roundtrip all texture wrap modes
  (blender/blender-addons@70efc485eca8)
- Various Draco encoder fixes
  (blender/blender-addons@eb29a12da48e)
- fix collection offset when rotated or scaled
  (blender/blender-addons@3e7209f9f289)
- More fixes for changing the filename when format changed
  (blender/blender-addons@3ce41afdfa56)
