# Core

### Library Overrides

- Major improvements to the resyncing process:
  - When resyncing liboverrides, a missing root of the liboverride
    hierarchy (i.e. if the hierarchy root linked reference ID is
    missing) will prevent any attempt to resync any data in that
    liboverride hierarchy. This is done to prevent resync code breaking
    existing overrides upon missing linked data
    (blender/blender@5f67b25c28).
  - Fixed several issues with liboverride partial resync process which
    could lead to some needed resync to be skipped
    (blender/blender@9ea2170cce,
    blender/blender@ba9ca7afb6,
    blender/blender@5574a1bfc1,
    blender/blender@b724015840,
    blender/blender@370a2bb416).
  - Refactored parts of the liboverride partial resync process to
    address known error reports
    (blender/blender@40d79e3d2b,
    blender/blender@07dfc6eccc).
  - Potential local references to virtual linked liboverrides are now
    restored 'as best as possible' on resync (blender/blender!107144,
    blender/blender@527b21f0ae).
    Note that in general user should avoid such referencing to linked
    data generated from the recursive process, as it is by definition
    very 'unstable' and does not actually exist in any file storage.

### Geometry

- Data arrays are now shared between copies, resulting in an overall
  memory usage reduction of at least 25% with large geometries
  (blender/blender@7eee378eccc8f87e1330b9cfea2799928be9d657,
  blender/blender@dcb3b1c1f9cb3d5b5e1827a7378b909745c55d64).

### Collections

- A hash is now used for Collection object lookup, speeding up object
  linking & unlinking
  (blender/blender@ea97bb1641b9fc3424c0000c7c7db9a038ae6148).

### Custom Properties

- Float custom properties can now have sub-types (e.g., distance, angle
  etc.)
  (blender/blender@6e2721da30)
