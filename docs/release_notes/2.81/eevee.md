# Blender 2.81: Eevee

## Shadows

![New shadow defaults (32 samples)](../../images/Shadow_aa.png){style="width:400px;"}

Soft shadows were changed and now enabled with the `Use Soft Shadow`
option in the render properties Shadow panel. This will progressive
smooth shadows, in combination with a more conventional shadow filtering
method (PCF). Exponential and Variance Shadow Mapping methods were
removed.
(blender/blender@d8aaf25c23fa)

This improves shadow accuracy, makes light and shadow setup simpler, and
improves performance for soft shadows.

- Enabling the `Use Soft Shadow` option will now anti-alias the
  shadow-maps (even if the light size is 0).
- An automatic bias is computed based on the shadow map resolution.
  Increasing the shadow resolution will reduce the bias and thus light
  leaking.
- Reducing the shadow-map resolution will incidentally increase the
  filter size (previously done by increasing the light shadow's
  `Softness`) and will lead to less shadow banding. However, this is a
  global parameter and cannot be tweaked per light.
- The shadow `Bias` parameter now adds an additional bias to avoid
  false shadowing on depth discontinuity.

Other notable improvements:

- Contact shadows are now following the light shape. The contact shadow
  `Softness` has been removed.
- Sun lights now have their clip distances automatically computed.
- Instanced lights and shadow casters are now supported.

## Transparent BSDF

Transparency handling has been reworked and now supports the same BSDF
combinations as Cycles
(blender/blender@d5002f007e8d).

|Example of effect that could not be done with previous transparency|Associated node tree|
|![](../../images/Eevee2.81_transparent_bsdf1.png){style="width:420px;"}|![](../../images/Eevee2.81_transparent_bsdf3.png){style="width:534px;"}|

The Additive and Multiply blend mode have been replaced by shader
equivalent that is compatible with Cycles. Compatibility code has been
added to load blend file containing materials using these deprecated
blend modes, but there is a corner case where it can fail.
(blender/blender@13d469e6f0c5)

![Node setup replacing deprecated blend modes.](../../images/Eevee2.81_transparent_bsdf2.png){style="width:400px;"}

## Holdout

Holdout is now supported and works with both opaque and semi transparent
surfaces.
(blender/blender@67c10dbf1322)

![Holdout shaders examples.](../../images/Eevee2.81_holdout.png){style="width:400px;"}

## Other Changes

- Bump mapping was changed to give more accurate results closer to
  Cycles. In particular it makes bump mapping with procedural textures
  work much better than before, at the cost of some performance.
- The `Separate Albedo` option is now always on and removed from the
  UI. Mixing (the active) SSS node and other BSDFs is now more correct.
- Volumetric effects are now faster to compute on modern GPUs
  (blender/blender@caf494444cd7).
  A bug was fixed in the volumetric implementation which might slightly
  change the look of certain scenes.
