# Blender 2.81: Viewport

## Look Development

- Rendered shading mode option to use HDRI studio lights instead of
  scene lights and world, for both Cycles and Eevee. This mode can now
  be used for look development.
- LookDev mode was renamed to Material Preview. This mode will be aimed
  at interactive texturing and fast preview with Eevee.
- HDRI studio light strength can now be controlled in the shading
  popover.
- Cycles viewport option to display a render pass instead of the
  combined pass.

## Viewport

- Each viewport can have its own set of visible collections
  (blender/blender@92736a7b759).
- Workbench matcaps can now be OpenEXR images with separate `diffuse`
  and `specular` layers. The layer named `diffuse` will be affected
  by material, texture or random color, while `specular` will not.
- Mesh analysis overlay now supports meshes with modifiers, instead of
  only the original mesh.
- Image objects can now be set to display only in side views.

![Multi-layered matcaps (by Pablo Dobarro)](../../images/Demonstrating_Multi-layered_Matcaps_(Image_from_Pablo_Dobarro).png){style="width:480px;"}
