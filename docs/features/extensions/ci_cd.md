# CI/CD

It is possible to update an extension directly using the [API](https://extensions.blender.org/api/v1/swagger/#/extensions/extensions_versions_upload_create).

Code snippet:

``` { .sh .copy }
curl -X POST https://extensions.blender.org/api/v1/extensions/$EXTENSION/versions/upload/ \
  -H "Authorization:bearer $BLENDER_EXTENSIONS_TOKEN" \
  -F "version_file=@$FILENAME" \
  -F "release_notes=$RELEASE_NOTES"
```

Variables:

* <b>BLENDER_EXTENSION_TOKEN</b>: Authentication Token.
* <b>FILENAME</b>: the .zip package for the extension.
* <b>RELEASE_NOTES</b>: markdown release notes.
* <b>EXTENSION</b>: the extension id.

Example:

```
curl -X POST https://extensions.blender.org/api/v1/extensions/my_addon/versions/upload/ \
  -H "Authorization:bearer x_xRe91mBmlHLveA9v-xyKgPcm8Zig2EH7Uhh0vHCN8" \
  -F "version_file=@/home/user/my-addon-1.0.1.zip" \
  -F release_notes=$'* Fix crashes when opening Blender.\n* Improve performance 2x.\n* Fix parsing \\n characters.'
```

Authentication tokens can be generated from the [user profile page](https://extensions.blender.org/settings/tokens/) on the [extensions platform](https://extensions.blender.org/).