# 1.0.0

## Manifest

```toml
schema_version = "1.0.0"

# Example of manifest file for a Blender extension
# Change the values according to your extension
id = "my_example_extension"
version = "1.0.0"
name = "My Example Extension"
tagline = "This is another extension"
maintainer = "Developer name <email@address.com>"
# Supported types: "add-on", "theme"
type = "add-on"

# # Optional: link to documentation, support, source files, etc
# website = "https://extensions.blender.org/add-ons/my-example-package/"

# # Optional: tag list defined by Blender and server, see:
# # https://docs.blender.org/manual/en/dev/advanced/extensions/tags.html
# tags = ["Animation", "Sequencer"]

blender_version_min = "4.2.0"
# # Optional: Blender version that the extension does not support, earlier versions are supported.
# # This can be omitted and defined later on the extensions platform if an issue is found.
# blender_version_max = "5.1.0"

# License conforming to https://spdx.org/licenses/ (use "SPDX: prefix)
# https://docs.blender.org/manual/en/dev/advanced/extensions/licenses.html
license = [
  "SPDX:GPL-3.0-or-later",
]
# # Optional: required by some licenses.
# copyright = [
#   "2002-2024 Developer Name",
#   "1998 Company Name",
# ]

# # Optional: list of supported platforms. If omitted, the extension will be available in all operating systems.
# platforms = ["windows-x64", "macos-arm64", "linux-x64"]
# # Other supported platforms: "windows-arm64", "macos-x64"

# # Optional: bundle 3rd party Python modules.
# # https://docs.blender.org/manual/en/dev/advanced/extensions/python_wheels.html
# wheels = [
#   "./wheels/hexdump-3.3-py3-none-any.whl",
#   "./wheels/jsmin-3.0.1-py3-none-any.whl",
# ]

# # Optional: add-ons can list which resources they will require:
# # * files (for access of any filesystem operations)
# # * network (for internet access)
# # * clipboard (to read and/or write the system clipboard)
# # * camera (to capture photos and videos)
# # * microphone (to capture audio)
# #
# # If using network, remember to also check `bpy.app.online_access`
# # https://docs.blender.org/manual/en/dev/advanced/extensions/addons.html#internet-access
# #
# # For each permission it is important to also specify the reason why it is required.
# # Keep this a single short sentence without a period (.) at the end.
# # For longer explanations use the documentation or detail page.
#
# [permissions]
# network = "Need to sync motion-capture data to server"
# files = "Import/export FBX from/to disk"
# clipboard = "Copy and paste bone transforms"

# # Optional: advanced build settings.
# # https://docs.blender.org/manual/en/dev/advanced/extensions/command_line_arguments.html#command-line-args-extension-build
# [build]
# # These are the default build excluded patterns.
# # You only need to edit them if you want different options.
# paths_exclude_pattern = [
#   "__pycache__/",
#   "/.git/",
#   "/*.zip",
# ]

# This was added automatically when building and must not be included in source manifest files.
[build.generated]
platforms = ["windows-x64", "macos-arm64", "linux-x64", "windows-arm64", "macos-x64"]
```

### Required values

| Field | Description | Example |
| ----- | ----------- | ------- |
| <b>blender_version_min</b> | Minimum supported Blender version - use at least ``4.2.0``. | `"4.2.0"` |
| <b>id</b> | Unique identifier for the extension based on its name. | `"my_example_extension"` |
| <b>license</b> | List of [licenses](https://docs.blender.org/manual/en/dev/advanced/extensions/licenses.html), use [SPDX license identifier](https://spdx.org/licenses/). | `"SPDX:GPL-3.0-or-later"` |
| <b>maintainer</b> | Maintainer of the extension. | `"Developer Name <email@address.com>"` |
| <b>name</b> | Complete name of the extension. | `"My Example Extension"` |
| <b>schema_version</b> | Internal version of the file format. | `"1.0.0"` |
| <b>tagline</b> | One-line short description - cannot end with punctuation. | `"This is another extension"`|
| <b>type</b> | "add-on", "theme". | `"add-on"` |
| <b>version</b> | Version of the extension - must follow [semantic versioning](https://semver.org/). | `"1.0.0"` |

### Optional values

| Field | Description | Example |
| ----- | ----------- | ------- |
| <b>blender_version_max</b> | Blender version that the extension does not support, earlier versions are supported. | `"5.1.0"` |
| <b>website</b> | Website for the extension. | `"https://extensions.blender.org/add-ons/my-example-extension/"` |
| <b>copyright</b> | Some licenses require a copyright, copyrights must be "Year Name" or "Year-Year Name". | `["2002-2024 Developer Name", "1998 Company Name"]` |
| <b>tags</b> | List of tags. See the [list of available tags](https://docs.blender.org/manual/en/dev/advanced/extensions/tags.html). | `["Animation", "Sequencer"]` |
| <b>platforms</b> | List of supported platforms. If ommitted, the extension will be available in all operating systems. The available options are ["windows-x64", "windows-arm64", "macos-x64", "macos-arm64", "linux-x64"] | `["windows-amd64"]` |
| <b>wheels</b> | List of relative file-paths [Python Wheels](https://docs.blender.org/manual/en/dev/advanced/extensions/python_wheels.html). | `["./wheels/jsmin-3.0.1-py3-none-any.whl"]` |
| <b>permissions</b> | Add-ons can list which resources they require. The available options are: *files*, *network*, *clipboard*, *camera*, *microphone*. Each permission should be followed by an explanation (short single-sentence with no end punctuation). | `network = "Need to sync motion-capture data to server"` |

NOTE:
All the values present in the manifest file must be filled
(i.e., cannot be empty, nor text ``""``, nor list ``[]``).<br/>
If you don't want to set one of the optional values just exclude it from the manifest altogether.


### Build generated
`[build.generated]`

When building with `blender -c extension build --split-platforms`
a `build.generated` section is added with platform specific values.

## Changelog
* Initial version.
