# Asset Editing

Under certain conditions, it is possible to edit assets that are not stored in the currently open
file. This happens through special asset system managed files (as opposed to normal user managed
files), and the `BKE_asset_edit.hh` API. Every such editable asset is saved in a single such file,
together with its dependencies (and external files packed).

Only brush assets support this currently.

## Asset System Files (`.asset.blend` Files)

These files are indicated by a dedicated flag (`G_FILE_ASSET_EDIT_FILE`) stored in the file's
"global" data section, and use the `.asset.blend` file extension. Both the flag and the extension
need to be used for a file to be considered an asset system file. That means users may still use
the `.asset.blend` extension for normal files. It's just a useful indicator for them, but also so
Blender can tell if a file *may* be an asset file without having to open it it.

The design is to let the asset system regenerate these files when user's save changes to the
assets. This way we keep the updating simple, and avoid many possible issues coming from loading
and saving assets in normal, user managed .blend files. If users could open and overwrite these
special .asset.blend files, user created data would be stored in them that would be lost the next
time the asset system regenerates the files. That's why these files need to be special. Users can
open, but not save these files (but use "Save As" to create a new, normal file). Warnings are
displayed in the UI to notify the user about this.
