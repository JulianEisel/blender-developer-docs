# Adding a Geometry Node

The easiest way to get started with adding a new geometry node is to have a look at this
[patch](https://projects.blender.org/blender/blender/pulls/133045).

Once the basic boilerplate is in place, it's best to look at existing similar nodes and copy what
they are doing.
