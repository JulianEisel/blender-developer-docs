# Animation & Rigging: Bigger Projects

This document describes the longer-term, bigger-picture planning of what the Animation & Rigging
module want to focus on.

!!! Warning "Not a promise"
    **This is not a promise** of any concrete deliverable or even order in which things will be done.

    This is just a rough planning, and written down to be able to disuss & reprioritise things.


## Current Focus: Slotted Actions

Adding multi-datablock animation to the `Action`. Introduction of 'Slots', as well as the layered
animation data model (but limited to 1 layer and 1 infinite strip).

At the end of this phase, we should be confident enough in the design & implementation to take the
multi-datablock part out of experimental. This would include the single-layer/single-strip 'layered'
data model of the Action.

See: [#120406](https://projects.blender.org/blender/blender/issues/120406)

*Slotted Actions will be introduced in Blender 4.4.*

## Pose Library Improvements

- UI/UX polish, especially for creating & updating poses.
- Extensibility via extensions

## Rotation Modes Polishing

- Preference for default bone rotation mode.
- Expand Flip Quaternion operator to support objects as well.
- Operator to convert rotation modes, including animation (Rigify has one just for pose bones).


## Mini Map

New feature for Blender: a 'bone picker' that should be usable outside of just 'picking bones', and
thus gets another name: the mini map.

If not already implemented by the time we work on this, this should also include
[#120412: Adjusting hotkey behavior for bone (de)selection][120412].

Note that the plan for this is not there at all, beyond the general wish for this feature. In all
likelyness the project will consist of multiple phases to allow small steps between working, usable
states. These phases can then be interleaved with other work.

[120412]: https://projects.blender.org/blender/blender/issues/120412


## Layered Actions

Remove the restriction of having only 1 layer of animation in the `Action`. This also requires a
more elaborate UI to work with layered animation, and attention on many operators (to prevent issues
like [#122620: Breakdowner not Working Correctly with Layered Animation][122620]).

This cannot be done without replacing the current "animation filtering/channel definition" code.
This is a big piece of technical debt that needs replacing. This is a necessary precursor to layered
animation or rig nodes.

See [layered-anim].

[122620]: https://projects.blender.org/blender/blender/issues/122620
[layered-anim]: ../animation_system/layered.md

## Animation-level Constraints

Actions could have a layer that defines constraints. They are then instantiated on the affected
objects as soon as the Action is assigned, and removed when the Action is removed.

As a first milesone, the constraints themselves would be evaluated in exactly the same as they are
now.

## Animation Channel Selection Synchronisation

Basically solving [#71615: Select key in dopesheet deselect bone in the viewport][71615] and other
similar issues listed in [Weak Areas of the Animation System: Selection Syncing][weak-selection-sync].

This needs a proper design, and also should involve the Grease Pencil module.

[71615]: https://projects.blender.org/blender/blender/issues/71615
[weak-selection-sync]: https://developer.blender.org/docs/features/animation/module/weak_areas/#selection-synchronization-between-pose-bones-and-animation-channels

## Rig Nodes

Implement a rigging nodes system similar to Unreal Engine's Control Rig. Aside from that, Blender's
current constraints can be quite complex (in terms of number of options and how they interact), and
can likely benefit from separating them into a network of simpler nodes.

[Prototype Rignodes add-on](https://projects.blender.org/dr.sybren/rignodes)

## Face Maps

The ability to adjust bones without directly working with the bones, but
rather by interacting with groups mesh faces.

## Declarative Constraints

*(taking 'Copy Transforms' constraint as example here to simplify things)*

The current constraints are directional, and basically say "modify `A` by taking the transform of
`B`". Declarative constraints say "`A` and `B` should stick together". This would then be taken into
account by tooling, such that you can move `A` and it would also move `B`, and vice versa.

## Optimize VSE Playback for Animators

Animators often use the VSE to play reference footage during their work.
This task is a placeholder to look at improvements that can be done in
these specific setups to speed up the playback performance.

Historical link: This used to be tracked in
[T75719](https://developer.blender.org/T75719).

## Decouple physics and animation clock

This permits an interactive scene (e.g., VR) where physic simulation is
happening in realtime, without the animation playing together.

Historical link: This used to be tracked in
[T68979](https://developer.blender.org/T68979)

## Animation fine-tuning sculpting

Animation sculpting on top of finalized animation for fine tuned
animation details. This could even include sculpting on top of cached
geometry (like from USD or Alembic).

Historical link: This used to be tracked in
[T68906](https://developer.blender.org/T68906)

## Motion path viewport editing

Historical link: This used to be tracked in
[T68901](https://developer.blender.org/T68901)

For reference:

- [Editable Motion Trails 3D](https://blenderartists.org/t/editable-motion-trails-3d/1197906)
- [So I tried to implement “Tangent-Space Optimization for Interactive Animation Control”](https://blenderartists.org/t/so-i-tried-to-implement-tangent-space-optimization-for-interactive-animation-control/1230828/)
