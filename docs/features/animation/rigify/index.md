# Rigify Add-on API

## Introduction

Rigify is an auto-rigging add-on that allows a user to create an
armature by combining a number of components (called 'rigs') via a
'metarig' template. The components could represent parts like an 'arm',
'spine' or 'finger'.

For each used rig component the metarig must contain a set of bones that
define the shape of the part (the required number and structure being
specified by the individual rig), with one 'main' bone tagged with the
identifier of the rig and holding its parameter properties.

In order to generate the rig, the main engine of the Rigify addon
duplicates the metarig, scans it to determine the necessary rigs, loads
and instantiates their classes, and invokes their methods in the
appropriate order.

It is possible to use custom rig classes in addition to the ones
packaged with the add-on, via 'feature set' packages installed through
the add-on settings. These pages describe the Python APIs used to
implement such custom rigs.

## [Feature Set Package Structure](feature_sets.md)

Describes the correct organization of a feature set package.

## [Implementing a Rig Class](rig_class.md)

Describes how a rig component is implemented.

## [Main Generator Engine](generator.md)

Describes the main object that controls the generation process.

## [UI Script Generator](script_generator.md)

Describes the object implementing generation of the custom UI script.

## Utility Library

Rigify includes modules that provide a variety of utility function and
classes for use in rigs. This documents the most important ones.

[Bone Utilities (rigify.utils.bones)](utils/bones.md)  
Utilities for creation, placement and other manipulation of bones.

[Mechanism Utilities (rigify.utils.mechanism)](utils/mechanism.md)  
Utilities for creating custom properties, constraints and drivers.

[Naming Utilities (rigify.utils.naming)](utils/naming.md)  
Utilities for manipulating bone names.

Some legacy code imports functions from `rigify.utils`. That is
deprecated, and new code should always import directly from the relevant
sub-module.

## Rig-Specific Systems

Some rigs use library modules specific to them.

[Skin Rigs (rigify.rigs.skin)](skin.md)  
Modules implementing the skin rigging system for the new face.
