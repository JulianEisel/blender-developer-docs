# Bone Utilities (rigify.utils.bones)

This module provides utilities for working with bones.

## BoneDict class

This subclass of `dict` allows access to its contents via attribute
access syntax, in addition to the regular dictionary indexing.

It is intended for storing bone names, so it only allows storing
strings, lists and other BoneDicts. Assigning a regular `dict` will
automatically convert it into a `BoneDict`.

### Methods

`bones.flatten(key=None)`  
:   Returns all names in the dictionary as a flat list. The `key` argument
    can be used to only extract names stored under a specific key, e.g.
    `bones.flatten('ctrl')`.

## Main Bone Utilities

These functions and a mixin class used by BaseRig deal with creation of
bones.

### Functions

`get_bone(obj, name)`  
:   Returns an `EditBone` or `PoseBone`, depending on the currently
    active mode.

`new_bone(obj, name)`  
:   Creates a new bone with the given name, and returns the actual name of
    the new bone.

`copy_bone(obj, old_name, new_name, parent=False, inherit_scale=False, bbone=False, length=None, scale=None)`  
:   Copies a bone under a new name, including its location, orientation, and
    layers, and returns the actual new name.
    
    If `parent` is true, copies its parent, connect and inherit options.
    
    If `parent` or `inherit_scale` is true, copies the inherit scale
    option.
    
    If `bbone` is true, copies the edit mode B-Bone settings.
    
    The `length` parameter allows replacing the length after copy, while
    `scale` is multiplied.

`copy_bone_properties(obj, old_name, new_name, transforms=True, props=True, widget=True)`  
:   Copies Pose mode properties from the old to the new bone, including the
    rotation mode, locks and custom properties.

    The `transforms` parameter controls copying transforms and locks,
    `props` control custom properties, and `widget` controls the custom
    widget.

### BoneUtilityMixin class

This class provides bone creation and parenting tools as methods for the
class inheriting from it.

The inheriting class must have an `obj` field referencing the armature
object.

`self.register_new_bone(new_name, old_name=None)` *callback*  
:   This method should be overridden if the class needs to track newly
    created or copied bones, e.g. it's done by `BaseRig`. It is called by
    other methods of this class, but does nothing by default.

`self.get_bone(name)`  
:   Returns an `EditBone` or `PoseBone`, depending on the currently active mode.

`self.new_bone(new_name)`  
:   Creates a new bone, and registers it.

`self.copy_bone(old_name, new_name, parent=False, inherit_scale=False, bbone=False, length=None, scale=None)`  
:   Copies a bone using the `copy_bone` function described above, and registers it.

`self.copy_bone_properties(old_name, new_name, props=True, ui_controls=None, ...)`  
:   Copies Pose mode bone properties via the `copy_bone_properties`
    function described above.
    
    If `props` is true, `ui_controls` specifies whether to generate UI
    panel entries for the custom properties.
    
    If `ui_controls` is None, and `new_name` is a control, entries are
    associated with `new_name`.
    
    If `ui_controls` is True, the UI entries are associated with all
    controls of the rig.
    
    If `ui_controls` is False, no UI entries are generated.
    
    Otherwise `ui_controls` must be a bone name list.

`self.rename_bone(old_name, new_name)`  
:   Renames the bone, returning the actual new name, and registers it.

`self.get_bone_parent(name)`  
:   Returns the name of the parent of the specified bone, or None.

`self.set_bone_parent(name, parent_name, use_connect=False, inherit_scale=None)`  
:   Sets the parent of the specified bone. If `use_connect` is None, it is
    left unchanged. The `inherit_scale` parameter allows setting the
    Inherit Scale option.

`self.parent_bone_chain(name_list, use_connect=None, inherit_scale=None)`  
:   Connects the bones in the list into a parented chain, using
    `set_bone_parent`.

## Bone Placement

`flip_bone(obj, name)`  
:   Swaps the head and tail of the bone.

`flip_bone_chain(obj, name_list)`  
:   Flips a connected chain of bones, including its parent-child links. All
    children of the bones have `use_connect` cleared.

`put_bone(obj, name, pos, matrix=None, length=None, scale=None)`  
:   Moves the bone to the specified position, without changing orientation.

    The `matrix` parameter allows using a 3x3 or 4x4 matrix to also set
    the bone orientation. If a matrix is used, `pos` may be set to None to
    leave the position unchanged, or use the location from the 4x4 matrix.
    
    The `length` or `scale` parameters allow replacing or scaling the
    length of the bone.

`is_same_position(obj, name1, name2)`  
:   Checks if the heads of the bones are in the same location (with epsilon).

`is_connected_position(obj, name1, name2)`  
:   Checks if the tail of the first and the head of the second bone are in
    the same location (with epsilon).

`copy_bone_position(obj, old_name, new_name, length=None, scale=None)`  
:   Copies the position and orientation from the old to the new bone.
    Optionally replaces or scales the length.

`align_bone_orientation(obj, name, target_name)`  
:   Copies the orientation from the target bone.

`align_bone_roll(obj, name, target_name)`  
:   Copies the roll from the target bone.

`align_bone_x_axis(obj, name, vector)`  
:   Rolls the bone to aim the X axis in the specified direction.

`align_bone_z_axis(obj, name, vector)`  
:   Rolls the bone to aim the Z axis in the specified direction.

`align_bone_y_axis(obj, name, vector)`  
:   Rotates the bone to aim the Y axis in the specified direction.

`compute_chain_x_axis(obj, name_list)`  
:   Computes a vector perpendicular to the primary plane of the bone chain.

`align_chain_x_axis(obj, name_list)`  
:   Aligns the X axis of the bones to the result of
    `compute_chain_x_axis`.

`align_bone_to_axis(obj, name, axis, length=None, roll=0, flip=False)`  
:   Aligns the Y axis of the bone to the global axis
    ('x','y','z','-x','-y','-z'), optionally adjusting the length, flipping
    it before aligning, and setting the roll.

`set_bone_widget_transform(obj, bone_name, transform_bone, use_size=True, scale=1.0, target_size=False)`  
:   Assigns another bone to use for displaying the custom widget of the
    bone. The `transform_bone` may be Null or equal to `bone_name`.

    `use_size` specifies whether the widget should be scaled by the bone
    size.
    
    `target_size` specifies whether scaling should be based on the
    `transform_bone` rest size.
    
    `scale` specifies an additional widget scale factor.

## B-Bone Utilities

`disable_bbones(obj, name_list)`  
:   Clears the segment counts of all the bones to disable the B-Bone
    behavior.

`connect_bbone_chain_handles(obj, name_list)`  
:   Connects the bones into a chain via 'ABSOLUTE' custom handles.
