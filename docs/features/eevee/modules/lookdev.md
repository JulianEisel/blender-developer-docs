# Lookdev Module

Lookdev are a set of tools that help the artist to work on the lighting of the scene. In the shading
pop-over the world back-ground can be replaced by a HDRI that is configured in the user preferences.
Also a diffuse and reflective sphere can be rendered in the scene to help artist to understand the
local lighting based on 2 known materials.

## LookdevWorld

When syncing the world it is checked if an HDRI override is setup for the viewport (`Instance::use_studio_light()`).
When this is the case the scene world isn't synced, but the LookdevWorld is used
for drawing the background and extracting the world light to reflection probe.

```mermaid
classDiagram
    class World {
        sync()
    }

    class LookdevWorld {
        sync(new_parameters: LookdevParameters)
        world_get()
    }

    class LookdevParameters {
        hdri: std::string
        rot_z: float
        background_opacity: float
        intensity: float
        blur: float
    }

    World *--> LookdevWorld: lookdev_world_
```

`LookdevWorld` contains a world with a material for displaying a custom HDRI. During syncing
the used texture and other parameters are updated.


## Lookdev HDRI Spheres

```mermaid
classDiagram
    class LookdevModule {
        depth_tx_: Texture
        display_ps: PassSimple

        init(visible_rect)
        sync()
        draw()
        display()
    }

    class Sphere["LookdevModule::Sphere"] {
        framebuffer: Framebuffer
        color_tx: Texture
        pass: PassSimple
    }

    LookdevModule *--> Sphere: spheres_
```

`LookdevModule::draw` updates the internal sphere textures. `LookdevModule::display` displays the last textures on the active framebuffer
[LookdevView](../view.md#lookdev-view) is used to create an view from the orientation of the current viewport.

```mermaid
sequenceDiagram
    participant LookdevView
    participant LookdevModule
    participant Sphere

    activate LookdevView
    LookdevView --> LookdevModule: draw
        activate LookdevModule
        LookdevModule --> Sphere: bind_framebuffer
        LookdevModule --> Sphere: submit_pas

        deactivate LookdevModule
    LookdevView --> LookdevModule: display
        activate LookdevModule
        deactivate LookdevModule
    deactivate LookdevView

```


For performance reasons we reuse the last created shadow map.
This shadow maps might not have detailed information on the location the
spheres are positioned in the scene. To reduce these artifacts the spheres
are scaled to 1/100th scale of a single scene unit; making the spheres most
likely to be fully lit or in shadow.