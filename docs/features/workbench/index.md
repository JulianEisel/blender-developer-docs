# Workbench

Workbench is the draw engine that powers the solid display mode.

## Design principles

Workbench is meant to be very simple and optimized for large scene. Readability of the scene and reactivity being the main concern.

## Design Documents
- [Shadow](shadow.md)
