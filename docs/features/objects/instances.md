# Instances

User level information about instances can be found in the
[user manual](https://docs.blender.org/manual/en/latest/modeling/geometry_nodes/instances.html).

Currently instances are handled in two ways. The first is the older
"dupli" system, which creates a flat list of `DupliObject`. This is
communicated to renderers with the dependency graph iterator (See
`DEG_OBJECT_ITER_FOR_RENDER_ENGINE_FLAGS`). The render engines see
instances from this system as separate temporary objects with a link to
the original object they were generated from.

The second system is the `InstancesComponent` and `bke::Instances`,
which is a hierarchical list of geometry set references with
corresponding transforms. Currently these instances are converted to the
dupli system for renderers and the rest of Blender to use. However,
eventually there may be changes to generate a more efficient output for
renderers, such as a list of meshes combined with all of the transforms
that use them.
