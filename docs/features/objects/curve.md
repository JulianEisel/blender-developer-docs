# Curve Object

Currently the `Curve` object type is in the process of being replaced by the `Curves`
type. That task is tracked in blender/blender#68981.
