# Embedded IDs

Embedded IDs are data-blocks that do not exist in the [Main
data-base](main.md), but rather act as
sub-data of an owner ID, which is their only exclusive user.

Embedded IDs are flagged with `LIB_EMBEDDED_DATA`. `IDTypeInfo` of
the ID types that can be embedded data must implement the `owner_get`
callback to retrieve the owner ID from its embedded ID.
